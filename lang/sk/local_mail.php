<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local-mail
 * @author     Albert Gasset <albert.gasset@gmail.com>
 * @author     Marc Català <reskit@gmail.com> 
 * @translate     Dominik Zatkalík <zatkalik@hotmail.com> 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addbcc'] = 'Pridať skrytú kópiu';
$string['addcc'] = 'Pridať kópiu';
$string['addrecipients'] = 'Pridať adresátov';
$string['addto'] = 'Pridať komu';
$string['advsearch'] = 'Pokročile hladanie';
$string['all'] = 'Všetci';
$string['applychanges'] = 'Použiť zmeny';
$string['assigntonewlabel'] = 'Assign to a new label...';
$string['attachments'] = 'Prílohy';
$string['attachnumber'] = '{$a} prílohy';
$string['bcc'] = 'Skrytá kópia';
$string['bulkmessage'] = 'With selected users send a local mail...';
$string['cancel'] = 'Zrušiť';
$string['cannotcompose'] = 'Nemožete vytvoruť správu, pretože nie ste zapísaný do žiadneho kurzu.';
$string['cc'] = 'Kópia';
$string['compose'] = 'Nová správa';
$string['configenablebackup'] = 'Zalohovať / obnoviť';
$string['configenablebackupdesc'] = 'Povoliť zálohu a obnovenie mailových správ a menoviek.';
$string['configlegacynav'] = 'Legacy navigation';
$string['configlegacynavdesc'] = 'Use the legacy navigaton style. Not recommended for Moodle 3.2 or newer versions. It will be deleted in version 2.0 of the plugin.';
$string['continue'] = 'Pokračovať';
$string['courses'] = 'Kurzy';
$string['delete'] = 'Vymazať';
$string['discard'] = 'Zahodiť';
$string['downloadall'] = 'Stiahnuť všrtky';
$string['draft'] = 'Rozpísana';
$string['drafts'] = 'Rozpísane';
$string['editlabel'] = 'Editovať menovku';
$string['emptyrecipients'] = 'Žiaden príjemca.';
$string['emptytrash'] = 'Vyprázdni kôš';
$string['emptytrashconfirm'] = 'You are about to empty trash';
$string['erroremptycourse'] = 'Prosím špecifikujte kurz.';
$string['erroremptylabelname'] = 'Prosím špecifikujte meno menovky.';
$string['erroremptyrecipients'] = 'Prosím špecifikujte aspoň jedneho príjemcu.';
$string['erroremptysubject'] = 'Prosím zadajte prdmet správy.';
$string['errorrepeatedlabelname'] = 'Menovka s týmto názvom už existuje';
$string['errorinvalidcolor'] = 'Neplatná farba';
$string['filterbydate'] = 'Dítum (up to the day):';
$string['from'] = 'Od';
$string['hasattachments'] = '(Správa s prílohami)';
$string['inbox'] = 'Prijaté';
$string['invalidlabel'] = 'Neplatná menovka';
$string['invalidmessage'] = 'Neplatná správa';
$string['labeldeleteconfirm'] = 'Ste si istý že chcete vymazať menovku  \'{$a}\'';
$string['labelname'] = 'Meno';
$string['labelcolor'] = 'Farba';
$string['labels'] = 'Menovky';
$string['mail:addinstance'] = 'Pridať novú správu';
$string['mail:mailsamerole'] = 'Poslať správy používateľom s rovnakou rolou';
$string['mail:usemail'] = 'Use mail';
$string['mailmenu'] = 'Mail menu';
$string['mailupdater'] = 'Mail updater';
$string['markasread'] = 'Označiť ako prečítane';
$string['markasread_help'] = 'Ak je povolené, všetky nové spravy budú označené ako prečítane';
$string['markasstarred'] = 'Označiť hviezdičkou';
$string['markasunread'] = 'Označiť ako neprečítane';
$string['markasunstarred'] = 'Odobrať hviezdičku';
$string['maxattachments'] = 'Maximálny počet príloh';
$string['maxattachmentsize'] = 'Maximálna veľkosť prílohy';
$string['message'] = 'Správa';
$string['messagedeleteconfirm'] = 'Ste si istý že chcete zmazať správu  \'{$a}\'';
$string['messagesdeleteconfirm'] = 'Ste si istý že chcete zmazať   {$a} správu(y)';
$string['messageprovider:mail'] = 'Mail received notification';
$string['moreactions'] = 'Viac';
$string['mymail'] = 'My mail';
$string['newlabel'] = 'Nová menovka';
$string['nocolor'] = 'Žiadna farba';
$string['nolabels'] = 'Žiadne dostupné menovky.';
$string['nomessages'] = 'Žiadne správy.';
$string['nomessageserror'] = 'Akcia vyžaduje zvoliť aspoň jednu správu';
$string['nomessagestoview'] = 'Žiadne správy na zobrazenie.';
$string['none'] = 'None';
$string['norecipient'] = '(žiaden príjemca)';
$string['noselectedmessages'] = 'Žiadna zvolena správa';
$string['nosubject'] = '(bez predmetu)';
$string['notificationbody'] = '- Od: {$a->user}

- Predmet: {$a->subject}

{$a->content}';
$string['notificationbodyhtml'] = '<p>Od: {$a->user}</p><p>Predmet: <a href="{$a->url}">{$a->subject}</a></p><p>{$a->content}</p>';
$string['notificationpref'] = 'Poslať notifikácie';
$string['notificationsubject'] = 'Nová správa v {$a}';
$string['notingroup'] = 'Nieste účastnik žiadnej skupiny';
$string['pagingsingle'] = '{$a->index} z {$a->total}';
$string['pagingmultiple'] = '{$a->first}-{$a->last} z {$a->total}';
$string['perpage'] = 'Zobraz {$a} správ';
$string['pluginname'] = 'Správy';
$string['read'] = 'Čítať';
$string['references'] = 'Referencie';
$string['removelabel'] = 'Odstrániť menovku';
$string['reply'] = 'Odpovedať';
$string['replyall'] = 'Odpovedať všetkým';
$string['restore'] = 'Obnoviť';
$string['search'] = 'Hľadať';
$string['searchbyunread'] = 'Iba nečítane';
$string['searchbyattach'] = 'S prílohou';
$string['shortaddbcc'] = 'Skrytá kópia';
$string['shortaddcc'] = 'Kópia';
$string['shortaddto'] = 'Komu';
$string['showlabelmessages'] = 'Show "{$a}" label messages';
$string['showrecentmessages'] = 'Zobraziť aktuálne správy';
$string['smallmessage'] = '{$a->user} has sent you an email';
$string['toomanyrecipients'] = 'Vyhľadávanie ma priveľa výsledkov';
$string['forward'] = 'Poslať ďalej';
$string['save'] = 'Uložiť';
$string['send'] = 'Poslať';
$string['sendmessage'] = 'Poslať správu';
$string['sentmail'] = 'Odoslané';
$string['setlabels'] = 'Menovky';
$string['starred'] = 'Označné hviezdičkou';
$string['starredmail'] = 'Označené hviezdičkou';
$string['subject'] = 'Predmet';
$string['to'] = 'Komu';
$string['togglemailmenu'] = 'Prepnúť menu správ';
$string['trash'] = 'Kôš';
$string['undo'] = 'Späť';
$string['undodelete'] = '{$a} správ bolo presunutých do koša';
$string['undorestore'] = '{$a} správ bolo obnovených';
$string['unread'] = 'Nečítane';
$string['unstarred'] = 'Zrušiť hviezdičku';
